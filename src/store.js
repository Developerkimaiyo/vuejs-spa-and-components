export default {
  interns: [
    {
      id: 1,
      name: 'Maxwell Kimaiyo',
      slug: 'maxwell',
      chapter: 'Mobile Developer',
      stacks: [
        {
          name: 'Go'
        },
        {
          name: 'Flutter'
        }
      ]
    },
    {
      id: 2,
      name: 'Stacy Chebet',
      slug: 'stacy',
      chapter: 'Middleware Developer',
      stacks: [
        {
          name: 'Adonis'
        },
        {
          name: 'Flutter'
        }
      ]
    },
    {
      id: 3,
      name: 'Dorcas Cherono',
      slug: 'dorcas',
      chapter: 'Frontend Developer',
      stacks: [
        {
          name: 'Vue'
        },
        {
          name: 'Adonis'
        }
      ]
    },
    {
      id: 4,
      name: 'Gill Erick',
      slug: 'gill',
      chapter: 'Backend Developer',
      stacks: [
        {
          name: 'Vue'
        },
        {
          name: 'Quarkus'
        }
      ] 
    }
  ]
}
