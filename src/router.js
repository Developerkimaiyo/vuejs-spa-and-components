import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
Vue.use(Router)

const router = new Router({
  mode: 'history',
  linkExactActiveClass: 'vue-school-active-class',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      props: true
    },
    {
      path: '/interns',
      name: 'Interns',
      props: true,
      component: () => import(/* webpackChunkName: "Details"*/ './views/Interns')
    },
    {
      path: '/details/:slug',
      name: 'Details',
      props: true,
      component: () => import(/* webpackChunkName: "Details"*/ './views/Details')
    },

  ]
})


export default router
